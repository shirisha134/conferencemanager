import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class conferenceManagerTest {
  private List<Talk> inputList = new ArrayList<>();
  private List<Track> tracks = new ArrayList<>();
  private List<Track> output = new ArrayList<>();

  @Before
  public void setUp() throws InvalidTalkDuration {
    inputList.add(Talk.createValidTalk(60));
    inputList.add(Talk.createValidTalk(45));
    inputList.add(Talk.createValidTalk(30));
    inputList.add(Talk.createValidTalk(45));
    inputList.add(Talk.createValidTalk(45));
    inputList.add(Talk.createValidTalk(5));
    inputList.add(Talk.createValidTalk(60));
    inputList.add(Talk.createValidTalk(45));
    inputList.add(Talk.createValidTalk(30));
    inputList.add(Talk.createValidTalk(30));
    inputList.add(Talk.createValidTalk(45));
    inputList.add(Talk.createValidTalk(60));
    inputList.add(Talk.createValidTalk(60));
    inputList.add(Talk.createValidTalk(45));
    inputList.add(Talk.createValidTalk(30));
    inputList.add(Talk.createValidTalk(30));
    inputList.add(Talk.createValidTalk(60));
    inputList.add(Talk.createValidTalk(30));
    inputList.add(Talk.createValidTalk(30));

    Session session1 = Session.createMorningSession(Arrays.asList(Talk.createValidTalk(60), Talk.createValidTalk(45), Talk.createValidTalk(30), Talk.createValidTalk(45)));
    Session session2 = Session.createAfterNoonSession(Arrays.asList(Talk.createValidTalk(45), Talk.createValidTalk(5), Talk.createValidTalk(60), Talk.createValidTalk(45), Talk.createValidTalk(30), Talk.createValidTalk(30)));
    Session session3 = Session.createMorningSession(Arrays.asList(Talk.createValidTalk(45), Talk.createValidTalk(60), Talk.createValidTalk(60)));
    Session session4 = Session.createAfterNoonSession(Arrays.asList(Talk.createValidTalk(45), Talk.createValidTalk(30), Talk.createValidTalk(30), Talk.createValidTalk(60), Talk.createValidTalk(30), Talk.createValidTalk(30)));

    Track track1 = new Track(Arrays.asList(session1, session2));
    Track track2 = new Track(Arrays.asList(session3, session4));
    output.add(track1);
    output.add(track2);
  }

  @Test
  public void shouldBeAbleToCalculateNumberOfTracksNeeded() throws InvalidTalkDuration {
    ConferenceManager confManager = new ConferenceManager(tracks);
    assertEquals(2, confManager.numberOfTracksRequire(inputList));
  }

  @Test
  public void shouldBeAbleToPopulateTracksWithTalks() throws InvalidTalkDuration {
    ConferenceManager conferenceManager = new ConferenceManager(tracks);
    assertEquals(output, conferenceManager.populateTracks(inputList));
  }
}

