import org.junit.Assert;
import org.junit.Test;

public class TalkTest {
  @Test
  public void shouldCreateATalkWithValidDurationOnly() throws InvalidTalkDuration {
    Talk talk = Talk.createValidTalk(100);
    Assert.assertEquals(" 100", talk.toString());
  }
}
