import java.util.ArrayList;
import java.util.List;

/**
 * This track holds two sessions.
 */
public class Track {
  private List<Session> sessions;

  Track(List<Session> sessions) {
    this.sessions = sessions;
  }

  public Track() {
    this(new ArrayList<>());
  }

  public void addSessions(List<Session> sessions) {
    this.sessions = sessions;
  }

  @Override
  public String toString() {
    return sessions + "";
  }

  public Track getTrack(List<Talk> inputList) throws InvalidTalkDuration {
    this.addSessions(Session.getSessions(inputList));
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Track track = (Track) o;

    return sessions != null ? sessions.equals(track.sessions) : track.sessions == null;

  }

  @Override
  public int hashCode() {
    return sessions != null ? sessions.hashCode() : 0;
  }

  public List<Session> getSessions() {
    return sessions;
  }
}
