import java.util.List;

/**
 * Manager manages all the Tracks with two sessions each having multiple talks.
 */
class ConferenceManager {
  private List<Track> tracks;

  ConferenceManager(List<Track> tracks) {
    this.tracks = tracks;
  }

  int numberOfTracksRequire(List<Talk> inputList) throws InvalidTalkDuration {
    return Talk.numberOfTracks(inputList);
  }

  List<Track> populateTracks(List<Talk> inputList) throws InvalidTalkDuration {
    int numberOfTrackRequired = numberOfTracksRequire(inputList);
    for (int number = 1; number <= numberOfTrackRequired; number++) {
      this.tracks.add(new Track().getTrack(inputList));
    }
    return this.tracks;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ConferenceManager that = (ConferenceManager) o;

    return tracks != null ? tracks.equals(that.tracks) : that.tracks == null;

  }

  @Override
  public int hashCode() {
    return tracks != null ? tracks.hashCode() : 0;
  }
}
