import java.util.ArrayList;
import java.util.List;

/**
 * This drives the conference manager.
 */
public class Driver {
  private static List<Talk> inputList = new ArrayList<>();
  
  public static void setUp() throws InvalidTalkDuration {
    inputList.add(Talk.createValidTalk("Writing Fast Tests Against Enterprise Rails ", 60));
    inputList.add(Talk.createValidTalk("Overdoing it in Python", 45));
    inputList.add(Talk.createValidTalk("Lua for the Masses", 30));
    inputList.add(Talk.createValidTalk("Ruby Errors from Mismatched Gem Versions", 45));
    inputList.add(Talk.createValidTalk("Common Ruby Errors", 45));
    inputList.add(Talk.createValidTalk("Rails for Python Developers lightning", 5));
    inputList.add(Talk.createValidTalk("Communicating Over Distance", 60));
    inputList.add(Talk.createValidTalk("Accounting-Driven Development", 45));
    inputList.add(Talk.createValidTalk("Woah", 30));
    inputList.add(Talk.createValidTalk("Sit Down and Write", 30));
    inputList.add(Talk.createValidTalk("Pair Programming vs Noise", 45));
    inputList.add(Talk.createValidTalk("Rails Magic", 60));
    inputList.add(Talk.createValidTalk("Ruby on Rails: Why We Should Move On", 60));
    inputList.add(Talk.createValidTalk("Clojure Ate Scala (on my project)", 45));
    inputList.add(Talk.createValidTalk("Programming in the Boondocks of Seattle", 30));
    inputList.add(Talk.createValidTalk("Ruby vs. Clojure for Back-End Development", 30));
    inputList.add(Talk.createValidTalk("Ruby on Rails Legacy App Maintenance", 60));
    inputList.add(Talk.createValidTalk("A World Without HackerNews", 30));
    inputList.add(Talk.createValidTalk("User Interface CSS in Rails Apps", 30));
  }

  public static void main(String[] args) throws InvalidTalkDuration {
    Driver driver = new Driver();
    driver.setUp();
    List<Track> tracks = new ArrayList<>();
    ConferenceManager conferenceManager = new ConferenceManager(tracks);
    conferenceManager.populateTracks(inputList);
    int trackNumber = 1;
    for (int trackIndex = 0; trackIndex < tracks.size(); trackIndex++) {
      int startTime = 540;
      System.out.println("Track : " + trackNumber++);

      for (int j = 0; j < tracks.get(trackIndex).getSessions().size(); j++) {
        for (int k = 0; k < tracks.get(trackIndex).getSessions().get(j).getTalks().size(); k++) {
          int hours = startTime / 60;
          int minutes = startTime % 60;

          if (hours < 12) {
            System.out.printf("%d:%02d ", hours, minutes);
            System.out.println(tracks.get(trackIndex).getSessions().get(j).getTalks().get(k).getName());
            startTime += tracks.get(trackIndex).getSessions().get(j).getTalks().get(k).getDuration();
            continue;
          }

          if (hours >= 12 && hours < 13) {
            System.out.println("12.00 Lunch");
          }

          startTime = showAfterNoonList(j, k, trackIndex, tracks);
          System.out.printf("%d:%02d ", startTime / 60, startTime % 60);
          System.out.println("NetWorking Events");
          break;
        }
      }
      System.out.println("---------------------------------------");
    }
  }

  private static int showAfterNoonList(int sessionIndex, int talkIndex, int trackIndex, List<Track> tracks) {
    int startTime = 780;
    for (int jj = sessionIndex; jj < tracks.get(trackIndex).getSessions().size(); jj++) {
      for (int kk = talkIndex; kk < tracks.get(trackIndex).getSessions().get(jj).getTalks().size(); kk++) {
        int hours = startTime / 60;
        int minutes = startTime % 60;

        System.out.printf("%d:%02d ", hours, minutes);
        System.out.println(tracks.get(trackIndex).getSessions().get(jj).getTalks().get(kk).getName());
        startTime += tracks.get(trackIndex).getSessions().get(jj).getTalks().get(kk).getDuration();

      }
    }
    return startTime;
  }
}
