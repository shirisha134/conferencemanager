import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Session describes a time SESSION_DURATION where conference events are held.
 */
public class Session {
  private SessionType duration;
  private List<Talk> talksList = new ArrayList<>();

  List<Talk> getTalks() {
    return talksList;
  }

  private enum SessionType {
    MORNING_SESSION_DURATION(180),
    AFTERNOON_SESSION_DURATION(240);
    private int duration;
    SessionType(int duration) {
      this.duration = duration;
    }
  }

  private Session(SessionType duration, List<Talk> talksList) {
    this.duration = duration;
    this.talksList = talksList;
  }

  static Session createAfterNoonSession(List<Talk> talkList) {
    return new Session(SessionType.AFTERNOON_SESSION_DURATION, talkList);
  }

  static Session createMorningSession(List<Talk> talkList) {
    return new Session(SessionType.MORNING_SESSION_DURATION, talkList);
  }

  static List<Session> getSessions(List<Talk> inputList) throws InvalidTalkDuration {
    List<Session> sessionsList = new ArrayList<>();
    sessionsList.add(getASession(inputList, SessionType.MORNING_SESSION_DURATION));
    sessionsList.add(getASession(inputList, SessionType.AFTERNOON_SESSION_DURATION));
    return sessionsList;
  }

  private static Session createASession(SessionType sessionType) {
    return new Session(sessionType, new ArrayList<>());
  }

  private static Session getASession(List<Talk> inputList, SessionType sessionType) throws InvalidTalkDuration {
    Session session = createASession(sessionType);
    session.accommodateTalks(inputList, sessionType);
    return session;
  }

  private void accommodateTalks(List<Talk> inputList, SessionType sessionType) throws InvalidTalkDuration {
    int availableDuration = sessionType.duration;
    Iterator iterator = inputList.iterator();
    while (iterator.hasNext()) {
      Talk talk = (Talk) iterator.next();
      if (talk.isLessThan(availableDuration)) {
        this.talksList.add(talk);
        availableDuration -= talk.getDuration();
        iterator.remove();
      }
      if (availableDuration == 0) {
        break;
      }
    }
  }

  @Override
  public String toString() {
    return talksList + "";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Session session = (Session) o;

    return talksList != null ? talksList.equals(session.talksList) : session.talksList == null;

  }

  @Override
  public int hashCode() {
    return talksList != null ? talksList.hashCode() : 0;
  }
}
