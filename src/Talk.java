import java.util.List;

/**
 * Talk models a Event in a Big programming Conference.
 */
public class Talk {
  private final String name;
  private final int durationInMinutes;
  private boolean status;

  private Talk(String name, int durationInMinutes, boolean status) {
    this.name = name;
    this.durationInMinutes = durationInMinutes;
    this.status = status;
  }

  private Talk(String name, int durationInMinutes) {
    this(name, durationInMinutes, false);
  }

  static Talk createValidTalk(int duration) throws InvalidTalkDuration {
    return createValidTalk("", duration);
  }

  static Talk createValidTalk(String name, int duration) throws InvalidTalkDuration {
    if (isValid(duration)) {
      return new Talk(name, duration);
    }
    throw new InvalidTalkDuration("Invalid Duration of Talk");
  }

  private static boolean isValid(int duration) {
    return (duration > 0) && (duration < 240);
  }

  private static int totalDuration(List<Talk> inputList) {
    int totalDuration = 0;
    for (Talk talk : inputList) {
      totalDuration += talk.durationInMinutes;
    }
    return totalDuration;
  }

  static int numberOfTracks(List<Talk> inputList) {
    return (totalDuration(inputList) / 420) + 1;
  }

  boolean isLessThan(int availableDuration) {
    return this.durationInMinutes <= availableDuration;
  }

  int getDuration() {
    return durationInMinutes;
  }

  String getName() {
    return name;
  }

  @Override
  public String toString() {
    return name + " " + durationInMinutes + "";
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Talk talk = (Talk) o;

    return durationInMinutes == talk.durationInMinutes;

  }

  @Override
  public int hashCode() {
    return durationInMinutes;
  }
}
